enum Tile {
	BLANK("_"),
	HIDDEN_WALL("_"),
	WALL("W"),
	CASTLE("C");
	
	private String name;
	
	Tile(String name) {
        this.name = name;	
    }
	public String getName () {
		return name; 
	} 
	
}
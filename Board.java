import java.util.Random;

public class Board {
	private Tile[][] grid; 
	final int size = 5;
	
		public Board() {
        grid = new Tile[size][size];
		Random rng = new Random();
        for (int r = 0;r < grid.length; r++) {
            for (int c = 0; c < grid[r].length; c++) {
				int randomPosition = rng.nextInt(grid[r].length);
                grid[r][c] = Tile.BLANK;
				grid[r][randomPosition] = Tile.HIDDEN_WALL;
            }
        }
		
    }
	@Override
	public String toString() {
		String result = "";

    	for (Tile[] r : grid) {
			for (Tile tile : r) {
				result += tile.getName() + " ";
       	 	}
			result += "\n";
   	 	}
		return result;
	}

	public int placeToken (int row, int col) {
		if (row < 0 || row >= size|| col < 0 || col >= size){
			return -2;
			
		} else if ((grid[row][col] == Tile.CASTLE || grid[row][col] == Tile.WALL )) {
        	return -1;
			
		} else if ((grid[row][col] == Tile.HIDDEN_WALL)) {	
        	return 1;
			
		} else {
			grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}
	
}

		
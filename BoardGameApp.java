import java.util.Scanner;

public class BoardGameApp {
    public static void main(String[] args) {
	
		Board board = new Board();
			
		System.out.println("Let's play!");
		int numCastles = 5;
		int turns =8;
			
		while (numCastles > 0 && turns > 0  ) {
			
			System.out.println(board);
			System.out.println("Castles: " + numCastles);
			System.out.println("Turns: " + turns);
				
			Scanner scanner = new Scanner(System.in);
			System.out.print("Player, enter the row (1-5): ");
			int row = scanner.nextInt() -1;
			System.out.print("Player, enter the column (1-5): ");
			int col = scanner.nextInt() -1;
			int input = board.placeToken(row, col);
				
			if ( input < 0 ) {
					System.out.println("Invalid input! Please re-enter row and column values.");
				} else if (input == 1) {
					System.out.println("There's a wall in this position.");
					turns--;
					
				} else if (input == 0) {
					System.out.println("Castle has been successfully placed");
					turns--;
					numCastles--;
					
				}
				
				


		}
		
		System.out.println(board);
		
		if (numCastles == 0) {
			System.out.println("You won!!");
		} else {
			System.out.println("You lost:(");
		}
	
	}
}